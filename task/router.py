from .accounts.viewsets import UserViewSet
from rest_framework import routers
router = routers.DefaultRouter()

from .base.viewsets import PostsViewSet


router.register('users',UserViewSet,basename='users')


router.register('posts',PostsViewSet)








