from rest_framework import serializers
from rest_framework.serializers import ValidationError
from . models import posts,comments
from rest_framework.response import Response

from ..accounts.serializers import UserSerializer 
        

class postsSerializer(serializers.ModelSerializer):
    class Meta:
        model = posts
        fields = ('id','content','postedBy','label','link')
 



class commentsSerializer(serializers.ModelSerializer):

    class Meta:

        model = comments
        
        fields = ('id','comment','commentOn','commentedBy')
    









