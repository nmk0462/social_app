from django.contrib import admin
from .models import posts,comments
from django.contrib.auth.admin import UserAdmin

# Register your models here.

admin.site.register(posts)

admin.site.register(comments)


