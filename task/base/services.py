import random
import string
from collections import namedtuple
from functools import partial
from ..base import response
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from .models import posts,comments
from .serializers import commentsSerializer, postsSerializer

Post = namedtuple('posts', ['content', 'postedBy','link'])
Comment = namedtuple('comments',['comment','commentOn','commentedBy'])
def _parse_data(data, cls):

    serializer = cls(data=data)
    if not serializer.is_valid():
        raise ValidationError(serializer.errors)
    return serializer.validated_data


# Parse Auth login data
post_data = partial(_parse_data, cls=postsSerializer)
def post_creation(request):

    data = post_data(request.data)
    id=request.user.id
    id=posts.objects.all().count()
    user_data = Post(
        content=data.get('content'),
        postedBy=request.user.email,
        link="http://127.0.0.1:8000/api/posts/comment/{}".format(id+1)

    )
    x=posts()
    x.content=data.get('content')
    x.postedBy=request.user.email
    x.link="http://127.0.0.1:8000/api/posts/comment/{}".format(id+1)
    x.save()


    return postsSerializer(user_data).data

post_data1 = partial(_parse_data, cls=commentsSerializer)
def comment_creation(request,pk):

    data = post_data1(request.data)
    id=request.user.id
    user=posts.objects.get(id=pk)
    user_data1 = Comment(
        comment=data.get('comment'),
        commentOn=pk,
        commentedBy=request.user.email
        

    )
    x=comments()
    x.comment=data.get('comment')
    x.commentOn=pk
    x.commentedBy=request.user.email
    x.save()


    return commentsSerializer(user_data1).data