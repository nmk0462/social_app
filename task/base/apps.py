from django.apps import AppConfig


class BaseConfig(AppConfig):
    name = 'task.base'
