from .models import posts,comments
from .serializers import postsSerializer,commentsSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from datetime import date
import datetime
from ..accounts.models import User
from ..accounts.serializers import UserSerializer
from .services import comment_creation, post_creation
from . import response

class PostsViewSet(viewsets.ModelViewSet):
    queryset=posts.objects.all()
    serializer_class = postsSerializer

    def list(self, request):
        dataPosts=posts.objects.all()
        
        rr=[]
        for i in dataPosts:
            currentpost=posts.objects.filter(id=i.id)
            commentData=comments.objects.filter(commentOn=i.id)
            if len(commentData)<3:
                i.label='Silver'
            elif len(commentData)<6:
                i.label='Gold'
            else:
                i.label='Platinum'           
            i.save()
        
            serializer = postsSerializer(currentpost,many=True)
            serializer1=commentsSerializer(commentData,many=True)
            result=serializer.data+serializer1.data
            rr.append(result)



        return Response(rr)
    @action(detail=False,methods=['POST'])
    def create_post(self,request):
        data = post_creation(request)
        return response.Created(data)

    @action(methods=['GET', 'POST'],detail=False,url_path='comment/(?P<pk>[^/.]+)')
    def comment(self,request,pk=None):
        data=comment_creation(request,pk)
        
        return response.Created(data)
 



    
