import django_filters
from django.db.models import Count

from .models import User


class UserBasicFilter(django_filters.FilterSet):

    def custom_filter(self, queryset):
        return queryset.filter(role='Manager'
             
        )
    
    def count_filter(self,queryset):
        return  User.objects.values('role').annotate(count=Count('role'))

    def count_filter_gender(self,queryset):
        return  User.objects.values('gender').annotate(count=Count('gender'))

