from django.db.models import query,Count
from rest_framework.serializers import Serializer
from .models import  User,PasswordResetCode,personalDetails,familyMembers
from .serializers import UserSerializer,PasswordResetSerializer, familyDetailsSerializer,personalDetailsSerializer,PdfSerializer
from .services import auth_register_user,auth_login,auth_password_change, _parse_data,get_user_from_email_or_mobile,personaldata_creation,family_creation
from ..base import response
from django.conf import settings
from django.contrib.auth import logout, get_user_model
from django.core import signing
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action, permission_classes
from django.contrib.auth import logout, get_user_model
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from django.contrib.auth.decorators import login_required
import logging
from .services import  generate_pdf
from pprint import pprint
import os
import pythoncom
import string
import random
import json
import win32com.client as win32
from .permissions import accessPermission,accessPermission1
from functools import partial
from rest_framework.response import Response
from .filters import UserBasicFilter
from django_filters.rest_framework import DjangoFilterBackend
logger = logging.getLogger(__name__)

parse_password_reset_data = partial(_parse_data, cls=PasswordResetSerializer)
class UserViewSet(viewsets.ModelViewSet):
 
    queryset = User.objects.all()
    permission_classes_by_action = {"data_manager": [accessPermission],
                                      "data_general":[accessPermission1],}

    serializer_class = UserSerializer

    @action(detail=False,methods=['GET']) 
    def data_manager(self,request):
        
        queryset=User.objects.all()

        Ser=UserSerializer(queryset,many=True)
        return Response(Ser.data)

    @action(detail=False,methods=['GET']) 
    def data_general(self,request):
        
        query=User.objects.filter(email=request.user.email)

        Ser=UserSerializer(query,many=True)
        return Response(Ser.data)


    @action(detail=False, methods=['POST'])
    def register(self, request):
        data = auth_register_user(request)
        return response.Created(data)

    @action(detail=False, methods=['POST'])
    def login(self, request):
        return auth_login(request)

        
    @action(detail=False, methods=['POST'])
    def logout(self, request):
        try:
            data = parse_password_reset_data(request.data)
            email = data.get('email')
            data=Token.objects.filter(key=email)
            data.delete()
            logout(request)
            return response.Ok({"detail": "Successfully logged out."})
        except:
            return response.Ok({"detail": "No User found"})
    
    @action(detail=False, methods=['POST'])
    def password_change(self, request):
        data = auth_password_change(request)
        user, new_password = data.get('email'), data.get('new_password')
        check=User.objects.get(email=user)
        if check.check_password(data.get('old_password')):
            check.set_password(new_password)
            check.save()
            content = {'success': 'Password changed successfully.'}
            return response.Ok(content)
        else:
            content = {'detail': 'Old password is incorrect.'}
            return response.BadRequest(content)

    @action(detail=False, methods=['POST'])
    def reset_mail(self, request):
        data = parse_password_reset_data(request.data)
        email = data.get('email')
        user, email_user, mobile_user = get_user_from_email_or_mobile(email)
        if not email_user:
            return response.BadRequest({'detail': 'User does not exists.'})
        if user:
            try:
                email = user.email
                password_reset_code = PasswordResetCode.objects.create_reset_code(user)
                password_reset_code.send_password_reset_email()
                message = "We have sent a password reset link to the {}. Use that link to set your new password".format(
                    email)
                return response.Ok({"detail": message})
            except get_user_model().DoesNotExist:
                message = "Email '{}' is not registered with us. Please provide a valid email id".format(email)
                message_dict = {'detail': message}
                return response.BadRequest(message_dict)
            except Exception:
                message = "Unable to send password reset link to email-id- {}".format(email)
                message_dict = {'detail': message}
                logger.exception(message)
                return response.BadRequest(message_dict)
        else:
            message = {'detail': 'User for this staff does not exist'}
            return response.BadRequest(message)

    @action(detail=False, methods=['POST'])
    def reset_password(self, request):
        code = request.data.get('code')
        password = request.data.get('password')
        if code:
            try:
                l1=list(code.split("/"))
                password_reset_code = PasswordResetCode.objects.get(code=l1[1])
                
                password_reset_code.user = get_user_model().objects.get(email=password_reset_code.user)
            except:
                message = 'Unable to verify user.'
                message_dict = {'detail': message}
                return response.BadRequest(message_dict)
        

            password_reset_code.user.set_password(password)
            password_reset_code.user.save()
            message = "Password Created successfully"
            message_dict = {'detail': message}
            return response.Ok({"success": message_dict})
        else:
            message = {'detail': 'Password reset link expired. Please re-generate password reset link. '}
            return response.BadRequest(message)

    @action(detail=False,methods=['GET'])
    def count(self,request):
        queryset=User.objects.all()
        self.filterset_class = UserBasicFilter
        queryset = UserBasicFilter.count_filter(self,queryset)

        return Response({'count':queryset})

    @action(detail=False,methods=['GET'])
    def count_by_gender(self,request):
        queryset=User.objects.all()
        self.filterset_class = UserBasicFilter
        queryset = UserBasicFilter.count_filter_gender(self,queryset)

        return Response({'count':queryset})



    @action(detail=False,methods=['GET','POST'])
    def personaldetails(self,request):
        if request.method=='GET':
            queryset=personalDetails.objects.all()
            result=[]
            for i in queryset:
                if i.add=="Y":
                    userdata=User.objects.filter(email=i.username)
                    serializer1=UserSerializer(userdata,many=True)
                    personaldata=personalDetails.objects.filter(id=i.id)
                    serializer=personalDetailsSerializer(personaldata,many=True)

                    result=result+serializer.data+serializer1.data
                else:
                    personaldata=personalDetails.objects.filter(id=i.id)
                    serializer=personalDetailsSerializer(personaldata,many=True)

                    result=result+serializer.data                    


            
            return Response(result)
        else:
            try:
                check=personalDetails.objects.filter(username=request.user.email)
            except:
                check=None
            if check:
                message = {'detail': 'details already exists '}
                return response.BadRequest(message)
            data=personaldata_creation(request)
        
            return response.Created(data)

    @action(detail=False,methods=['GET','POST'])
    def familydetails(self,request):
        if request.method=='GET':
            queryset=familyMembers.objects.all()
            result=familyDetailsSerializer(queryset,many=True)
        
            return Response(result.data)
        else:
            data=family_creation(request)
        
            return response.Created(data)
    @action(methods=['GET'], detail=False)
    def get_pdf(self, request):
        query=User.objects.all()


        pdf_obj = generate_pdf(query,"pdf.html")

        result = PdfSerializer(pdf_obj).data
        return response.Ok(result)
    @action(methods=['GET'],detail=False)
    def get_excel(self,request):
        pythoncom.CoInitialize()

        json_data = UserSerializer(User.objects.all()).data




        rows = []

        for i in range(len(json_data['data'])):
            email = json_data['data'][i]['email']
            role = json_data['data'][i]['role']
            gender = json_data['data'][i]['gender']

            rows.append([email, role, gender])

        ExcelApp = win32.Dispatch('Excel.Application')
        ExcelApp.Visible = True

        wb = ExcelApp.Workbooks.Add()
        ws = wb.Worksheets(1)

        header_labels = ('email', 'role', 'gender')

        for indx, val in enumerate(header_labels):
            ws.Cells(1, indx + 1).Value = val

        row_tracker = 2
        column_size = len(header_labels)

        for row in rows:
            ws.Range(
                ws.Cells(row_tracker, 1),
                ws.Cells(row_tracker, column_size)
            ).value = row
            row_tracker += 1

  
        N = 5
  
        res = ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k = N))
        wb.SaveAs(os.path.join(os.getcwd()+'/media', res), 51)
        wb.Close()
        ExcelApp.Quit()
        ExcelApp = None
        return Response(json_data)



    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

   


